# This file is part of sch-scripts, https://sch-scripts.gitlab.io
# Copyright 2019-2020 the sch-scripts team, see AUTHORS
# SPDX-License-Identifier: GPL-3.0-or-later

# Sourced by all sch-scripts shell scripts, provides some common functions

# POSIX recommends that printf is preferred over echo.
# But do offer a simple wrapper to avoid "%s\n" all the time.
echo() {
    printf "%s\n" "$*"
}

bold() {
    if [ -z "$printbold_first_time" ]; then
        printbold_first_time=true
        bold_face=$(tput bold 2>/dev/null) || true
        normal_face=$(tput sgr0 2>/dev/null) || true
    fi
    printf "%s\n" "${bold_face}$*${normal_face}"
}

log() {
    logger -t sch-scripts -p syslog.err "$@"
}

# Try to find the user that ran sudo or su
detect_administrator() {
    _ADMINISTRATOR=$(id -un) || true
    if [ "${_ADMINISTRATOR:-root}" = "root" ]; then
        _ADMINISTRATOR=$(loginctl user-status | awk '{ print $1; exit 0 }') ||
            true
    fi
    if [ "${_ADMINISTRATOR:-root}" = "root" ]; then
        _ADMINISTRATOR=${SUDO_USER}
    fi
    if [ "${_ADMINISTRATOR:-root}" = "root" ] && [ "${PKEXEC_UID:-0}" != "0" ]
    then
        _ADMINISTRATOR=${PKEXEC_UID}
    fi
    if [ "${_ADMINISTRATOR:-root}" != "root" ]; then
        IFS=: read -r _ADMINISTRATOR _dummy _ADMIN_UID _ADMIN_GID _dummy <<EOF
$(getent passwd "$_ADMINISTRATOR")
EOF
    fi
    if [ "${_ADMINISTRATOR:-root}" = "root" ]; then
        unset _ADMINISTRATOR _ADMIN_UID _ADMIN_GID
    fi
    return 0
}

# Output a message to stderr and abort execution
die() {
    log "$@"
    bold "ERROR in ${0##*/}:" >&2
    echo "$@" >&2
    pause_exit 1
}

# Check if parameter is a command; `command -v` isn't allowed by POSIX
is_command() {
    local fun

    if [ -z "$is_command" ]; then
        command -v is_command >/dev/null ||
            die "Your shell doesn't support command -v"
        is_command=1
    fi
    for fun in "$@"; do
        command -v "$fun" >/dev/null || return $?
    done
}


# $search must be ready for sed, e.g. '^whole line$'.
# Return 0 if a replacement was made, 1 otherwise.
search_and_replace() {
    local search replace file backup comment
    search=$1
    replace=$2
    file=$3
    backup=$4

    if [ "${backup:-1}" = "1" ]; then
        comment=" # Commented by sch-scripts: &"
    else
        comment=""
    fi
    if grep -qs "$search" "$file"; then
        sed "s/$search/$replace$comment/" -i "$file"
        return 0
    fi
    return 1
}

# Create a relative symlink only if the destination directory exists,
# while overwriting existing symlinks, but not files/dirs.
symlink() {
    local dst src

    dst=$1
    src=$2
    test -d "${dst%/*}" || return 0
    test -h "$src" && rm "$src"
    if [ -e "$src" ]; then
        warn "Not overwriting $src with a symlink to $dst"
        return 0
    fi
    mkdir -p "${src%/*}" || return 0
    ln -rs "$dst" "$src"
}

# Print a message to stderr
warn() {
    printf "%s\n" "$*" >&2
}

_VERSION=$(. /usr/share/sch-scripts/version.py >/dev/null && printf "%s\n" "$__version__")
